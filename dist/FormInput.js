/** @format */
import React from "react";
function FormInput({ label, name, type, value, required, readonly, hidden, richBox, longBox, shortBox, max, min, disabled, handleChange, handleBlur, onKeyEvent, }) {
    return (React.createElement("div", { className: "form-input-container" },
        React.createElement("div", null,
            React.createElement("label", { htmlFor: name, className: "form-label", hidden: hidden },
                label,
                required ? React.createElement("span", { className: "form-required" }, "*") : null)),
        React.createElement("input", { max: max, min: min, name: name, type: type, placeholder: label, value: value, required: required, readOnly: readonly, className: `${richBox ? "rich-form-input" : ""}
        ${longBox ? "long-form-input" : ""}
        ${shortBox ? "short-form-input" : ""}
        form-input`, onChange: handleChange, onBlur: handleBlur, onKeyPress: onKeyEvent, hidden: hidden, disabled: disabled })));
}
export default FormInput;
