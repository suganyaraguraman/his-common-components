{
  "name": "his-common-component",
  "version": "1.0.1",
  "description": "To make it easy for you to get started with GitLab, here's a list of recommended next steps.",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build:css": "node-sass src/ -o dist/ ",
    "clean": "del dist",
    "build": "npm run clean && npm run build:css && tsc && cp package.json README.md"
  },
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/suganyaraguraman/his-common-components.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://gitlab.com/suganyaraguraman/his-common-components/issues"
  },
  "homepage": "https://gitlab.com/suganyaraguraman/his-common-components#readme",
  "devDependencies": {
    "@types/node-sass": "^4.11.3",
    "@types/react": "^18.0.17",
    "node-sass": "^7.0.1",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "typescript": "^4.7.4"
  }
}
