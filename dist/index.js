import CustomButton from "./CustomButton";
import FormInput from "./FormInput";
import "./custom-button.css";
import "./form-input.css";
export default { CustomButton, FormInput };

