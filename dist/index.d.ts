import CustomButton from "./CustomButton";
import FormInput from "./FormInput";
import "./custom-button.css";
import "./form-input.css";
declare const _default: {
    CustomButton: typeof CustomButton;
    FormInput: typeof FormInput;
};
export default _default;
