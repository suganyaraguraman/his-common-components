/** @format */
import React from "react";
function CustomButton({ children, type, isDisabled, isNormal, onClick, isHidden, }) {
    return (React.createElement("div", { className: "custom-button-container" },
        React.createElement("button", { className: `${isDisabled
                ? "custom-button-disabled"
                : isNormal
                    ? "custom-button-normal"
                    : "custom-button-submit"} custom-button`, type: type, disabled: isDisabled, onClick: onClick, hidden: isHidden }, children)));
}
export default CustomButton;
