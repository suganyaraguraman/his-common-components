/** @format */
import React, { PropsWithChildren } from "react";
interface IProps<T extends HTMLInputElement = HTMLInputElement> {
    label: string;
    name: string;
    type: string;
    value?: string | number;
    required?: boolean | undefined;
    readonly?: boolean | undefined;
    richBox?: boolean | undefined;
    longBox?: boolean | undefined;
    shortBox?: boolean | undefined;
    disabled?: boolean | undefined;
    hidden?: boolean | undefined;
    max?: string | undefined;
    min?: string | undefined;
    handleChange?: (event: React.ChangeEvent<T>) => void;
    handleBlur?: (event: React.FocusEvent<T>) => void;
    onChange?: (event: React.PointerEvent<HTMLInputElement>) => void;
    onfocus?: (event: React.FocusEvent<any>) => void;
    onKeyEvent?: (event: React.KeyboardEvent<any>) => void;
}
declare function FormInput({ label, name, type, value, required, readonly, hidden, richBox, longBox, shortBox, max, min, disabled, handleChange, handleBlur, onKeyEvent, }: PropsWithChildren<IProps>): JSX.Element;
export default FormInput;
