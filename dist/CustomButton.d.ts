/** @format */
import React, { PropsWithChildren } from "react";
interface IProps<T extends HTMLButtonElement = HTMLButtonElement> {
    type: "submit" | "button";
    isDisabled?: boolean | undefined;
    isNormal?: boolean | undefined;
    onClick?: (event: React.MouseEvent<T>) => void;
    isHidden?: boolean | undefined;
}
declare function CustomButton({ children, type, isDisabled, isNormal, onClick, isHidden, }: PropsWithChildren<IProps>): JSX.Element;
export default CustomButton;
