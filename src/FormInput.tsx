/** @format */

import React, { PropsWithChildren } from "react";

//import "./form-input.styles.scss";

interface IProps<T extends HTMLInputElement = HTMLInputElement> {
  label: string;
  name: string;
  type: string;
  value?: string | number;
  required?: boolean | undefined;
  readonly?: boolean | undefined;
  richBox?: boolean | undefined;
  longBox?: boolean | undefined;
  shortBox?: boolean | undefined;
  disabled?: boolean | undefined;
  hidden?: boolean | undefined;
  max?: string | undefined;
  min?: string | undefined;
  handleChange?: (event: React.ChangeEvent<T>) => void;
  handleBlur?: (event: React.FocusEvent<T>) => void;
  onChange?: (event: React.PointerEvent<HTMLInputElement>) => void;
  onfocus?: (event: React.FocusEvent<any>) => void;
  onKeyEvent?: (event: React.KeyboardEvent<any>) => void;
}

function FormInput({
  label,
  name,
  type,
  value,
  required,
  readonly,
  hidden,
  richBox,
  longBox,
  shortBox,
  max,
  min,
  disabled,
  handleChange,
  handleBlur,
  onKeyEvent,
}: PropsWithChildren<IProps>) {
  return (
    <div className="form-input-container">
      <div>
        <label htmlFor={name} className="form-label" hidden={hidden}>
          {label}
          {required ? <span className="form-required">*</span> : null}
        </label>
      </div>
      <input
        max={max}
        min={min}
        name={name}
        type={type}
        placeholder={label}
        value={value}
        required={required}
        readOnly={readonly}
        className={`${richBox ? "rich-form-input" : ""}
        ${longBox ? "long-form-input" : ""}
        ${shortBox ? "short-form-input" : ""}
        form-input`}
        onChange={handleChange}
        onBlur={handleBlur}
        onKeyPress={onKeyEvent}
        hidden={hidden}
        disabled={disabled}
      />
    </div>
  );
}

export default FormInput;
