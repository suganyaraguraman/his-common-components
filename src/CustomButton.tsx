/** @format */

import React, { PropsWithChildren } from "react";

//import "./custom-button.scss";

interface IProps<T extends HTMLButtonElement = HTMLButtonElement> {
  type: "submit" | "button";
  isDisabled?: boolean | undefined;
  isNormal?: boolean | undefined;
  onClick?: (event: React.MouseEvent<T>) => void;
  isHidden?: boolean | undefined;
}

function CustomButton({
  children,
  type,
  isDisabled,
  isNormal,
  onClick,
  isHidden,
}: PropsWithChildren<IProps>) {
  return (
    <div className="custom-button-container">
      <button
        className={`${
          isDisabled
            ? "custom-button-disabled"
            : isNormal
            ? "custom-button-normal"
            : "custom-button-submit"
        } custom-button`}
        type={type}
        disabled={isDisabled}
        onClick={onClick}
        hidden={isHidden}
      >
        {children}
      </button>
    </div>
  );
}

export default CustomButton;
